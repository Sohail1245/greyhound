﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dog_Race
{
    class Greyhound
    {
        // Creates integer variables to store the greyhound's starting position 
        private double _startingPosition;

        // Creates integer variables to store the length of the racetrack
        private double _racetrackLength;

        // Creates integer variables to store the greyhound's current position on the racetrack 
        private double _currentLocation = 0;

        // Randomizer to create and store a random number of steps for the greyhound to move.
        private Random _randomSteps;

        /// <summary>
        /// Greyhound constructor class
        /// </summary>
        /// <param name="startingPosition">The numeric value of the starting position of the greyhounds</param>
        /// <param name="racetrackLength">The numeric value of the length of the racetrack</param>
        /// <param name="currentLocation">The numeric value of the greyhound's current location</param>
        /// <param name="randomSteps">The numeric value of the random number of steps the greyhound will go</param>
        public Greyhound(double startingPosition, double racetrackLength, double currentLocation, Random randomSteps)
        {
            // Initialize all the greyhound field variables
            _startingPosition = startingPosition;
            _racetrackLength = racetrackLength;
            _currentLocation = currentLocation;
            _randomSteps = randomSteps;
        }

        /// <summary>
        /// Property to access the _startingPosition variable. 
        /// </summary>
        public double StartingPosition
        {
            get { return _startingPosition; }
            set { _startingPosition = value; }
        }

        /// <summary>
        /// Property to access the _racetrackLength variable. 
        /// </summary>
        public double RacetrackLength
        {
            get { return _racetrackLength; }
            set { _racetrackLength = value; }
        }

        /// <summary>
        /// Property to access the _currentLocation variable. 
        /// </summary>
        public double CurrentLocation
        {
            get { return _currentLocation; }
            set { _currentLocation = value; }
        }

        /// <summary>
        /// Property to access the _randomSteps variable. 
        /// </summary>
        public Random RandomSteps
        {
            get { return _randomSteps; }
        }
        


        /// <summary>
        /// Determine a random number of steps, Move the image by the random number of steps, return true if the race has been won.
        /// </summary>
        /// <returns></returns>
        public bool Run()
        {
            while (_currentLocation < RacetrackLength)
            {
                for (int _greyhoundIndex = 0; _greyhoundIndex < 4; _greyhoundIndex++)
                {
                    _currentLocation += _randomSteps.Next(1, 10);
                }
            }
            return true;
        }

        /// <summary>
        /// Reset greyhound location to start line.
        /// </summary>
        public void TakeStartingPosition()
        {
            _currentLocation = _startingPosition;
        }

    }
}
