﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dog_Race
{
    class Guy
    {
        // Creates a string variable to store the player's names
        private string _name;
        // Creates a instance of the bet class for the player
        private Bet _bet;
        // Creates a boolean to determine if the user has placed a bet.
        private bool _hasPlacedBet;
        // Creates an integer variable to store how much money the player has.
        private int _cash;

        /// <summary>
        /// Guy constructor class
        /// </summary>
        /// <param name="name">String that stores the current player's name</param>
        /// <param name="bet">Instance of a bet object related to the player</param>
        /// <param name="cash">Integer that stores the amount of cash the player has</param>
        public Guy(string name, bool hasPlacedBet, int cash)
        {
            // Initialize all the guy field variables
            _name = name;
            _hasPlacedBet = hasPlacedBet;
            _cash = cash;
        }

        /// <summary>
        /// Property to access the _name variable. 
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Prooperty to access the _hasPlacedBet variable.
        /// </summary>
        public bool HasPlacedBet
        {
            get { return _hasPlacedBet; }
            set { _hasPlacedBet = value; }
        }

        /// <summary>
        /// Property to access the _bet variable. 
        /// </summary>
        public Bet Bet
        {
            get { return _bet; }
            set { _bet = value; }
        }

        /// <summary>
        /// Property to access the _cash variable. 
        /// </summary>
        public int Cash
        {
            get { return _cash; }
            set { _cash = value; }
        }

        /// <summary>
        /// Place a new bet and store it in the player's bet field, return true if the player had enough money to bet.
        /// </summary>
        /// <param name="Amount"></param>
        /// <param name="Dog"></param>
        /// <returns></returns>
        public bool PlaceBet(int Amount, int Dog)
        {
            // Declare local variable to store the amount the player bet.
            int _amount = Amount;

            // Declare local variable to store which dog the player bet on.
            int _dog = Dog;

            // If the amount the player bet is less than or equal to the amount of cash they have, the program will place the bet by using the Bet class, and will return true. If the player does not have enough cash, the program will return flase.
            if (_amount <= _cash)
            {
                // Uses the Bet class to place a new _bet. 
                _bet = new Bet(_amount, _dog, this);
                HasPlacedBet = true;
                return HasPlacedBet;
            }
            else
            {
                HasPlacedBet = false;
                return HasPlacedBet;
            }
        }

        /// <summary>
        /// Display the amount the player is currently betting, and display the amount of cash they have remaining.
        /// </summary>
        public void UpdateBet()
        {
            _bet.GetDescription();

        }

        /// <summary>
        /// Reset the players bet so that it is equal to zero.
        /// </summary>
        public void ClearBet()
        {
            // Resets the players bet so that it is equal to zero.
            _bet.Amount = 0;
        }

        /// <summary>
        /// Use the bet object to increase the balance of the winner, and decrease the balance of the losers..
        /// </summary>
        /// <param name="Winner"></param>
        public void Collect(int Winner)
        {
            // If the winner chose the correct dog, increase their balacnce by the amount they bet.
            if (_bet.Dog == Winner)
            {
                _cash += _bet.Amount;
            }

            // Else decrease it by the amount they bet.
            else
            {
                _cash -= _bet.Amount;
            }
        }

    }
}
