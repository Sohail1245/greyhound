﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dog_Race
{
    class Bet
    {
        // Creates an integer variable to store the amount bet.
        private int _amount;

        // Creates an integer variable to store the dog the player bet on.
        private int _dog;

        // Creates an instance to store the player that is placing the bet.
        private Guy _bettor;

        /// <summary>
        /// Bet class constructor
        /// </summary>
        /// <param name="amount">Stores the amount the player bet</param>
        /// <param name="dog">Stores which dog the player bet on</param>
        /// <param name="bettor">Stores the instance of the player class that is using this classs</param>
        public Bet(int amount, int dog, Guy bettor)
        {
            //Initialize all the Bet field variables
            _amount = amount;
            _dog = dog;
            _bettor = bettor;
        }

        /// <summary>
        /// Property to access the _amount variable. 
        /// </summary>
        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        /// <summary>
        /// Property to access the _dog variable. 
        /// </summary>
        public int Dog
        {
            get { return _dog; }
            set { _dog = value; }
        }

        /// <summary>
        /// Property to access the _bettor variable. 
        /// </summary>
        public Guy Bettor
        {
            get { return _bettor; }
            set { _bettor = value; }
        }

        /// <summary>
        /// Return a string that says who placed the bet, how much cash was bet, and which dog he bet on. If the amount is 0 then a bet was not placed.
        /// </summary>
        /// <returns>_description</returns>
        public string GetDescription()
        {
            string _description = "";

            // If the amount the player bet is greater than 0, and less than or equal to the total amount of cash they have, then a valid bet was placed.
            if (_amount > 0)
            {
                // Provides string with the information of the player's name, the player's bet, and which dog they bet on.  
                _description = String.Format("{0} bets ${1} on dog #{2}", Bettor.Name, Amount, Dog);
            }
            // If the amount the player bet is equal than 0, then they have not placed a bet.
            else if (_amount == 0)
            {
                // Provides string with the information that the current player has not placed a bet.
                _description = String.Format("{0} has not placed a bet", Bettor.Name);
            }

            else if (_amount<0) 
            {
                // Provides string with the information that the current player has not placed a valid bet.
                _description = String.Format("{0} has entered an amount less than 0", Bettor.Name);
            }

            return _description;
        }

        /// <summary>
        /// The parameter is the winner of the race. If the player chose the correct dog as the winner, return the amount they bet(the amount they win). If the player chose the incorrect dog, return the negative of the amount they bet(the amount they will lose).
        /// </summary>
        /// <param name="Winner"></param>
        /// <returns></returns>
        public int PayOut(int Winner)
        {
            int _payOut = 0;

            //TODO:If the player chose the correct dog as the winner, return the amount they bet(the amount they win). 
            //TODO: If the player chose the incorrect dog, return the negative of the amount they bet(the amount they will lose).

            return _payOut;
        }

    }
}
