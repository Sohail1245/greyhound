﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Dog_Race
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //Creates a "random" variable to store a random number.
        private Random _randomizer;

        private Greyhound[] _greyhounds;

        private Guy[] _guys;

        public MainPage()
        {
            this.InitializeComponent();
            _randomizer = new Random();
            _guys = new Guy[3];
            _greyhounds = new Greyhound[4];
            CreateBettors();
            CreateRaceHounds();
        }

        /// <summary>
        /// Property to access the _random variable. 
        /// </summary>
        public Random Randomizer
        {
            get { return _randomizer; }
            set { _randomizer = value; }
        }

        /// <summary>
        /// Creates Greyhound objects
        /// </summary>
        public void CreateRaceHounds()
        {
            Greyhound greyHound1 = new Greyhound(0, 10, 0, _randomizer);
            _greyhounds[0] = greyHound1;
            Greyhound greyHound2 = new Greyhound(0, 10, 0, _randomizer);
            _greyhounds[1] = greyHound2;
            Greyhound greyHound3 = new Greyhound(0, 10, 0, _randomizer);
            _greyhounds[2] = greyHound3;
            Greyhound greyHound4 = new Greyhound(0, 10, 0, _randomizer);
            _greyhounds[3] = greyHound4;
        }

        /// <summary>
        /// Creates bettor objects
        /// </summary>
        public void CreateBettors()
        {
            Guy Joe = new Guy("Joe", false, 50);
            _guys[0] = Joe;
            Guy Bob = new Guy("Bob", false, 75);
            _guys[1] = Bob;
            Guy Al = new Guy("Al", false, 45);
            _guys[2] = Al;
        }

        /// <summary>
        /// Populates ComboBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLoad(object sender, RoutedEventArgs e)
        {
            _comboDogChoice.Items.Add("1");
            _comboDogChoice.Items.Add("2");
            _comboDogChoice.Items.Add("3");
            _comboDogChoice.Items.Add("4");
            _comboDogChoice.SelectedIndex = 0;
        }

        /// <summary>
        /// Determines player based on radio button selection and displays their name on the current player textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBettorSelectorClicked(object sender, RoutedEventArgs e)
        {
            if (sender == _radioJoe)
            {
                _txtBlockName.Text = "Name: Joe";
            }

            if (sender == _radioBob)
            {
                _txtBlockName.Text = "Name: Bob";
            }

            if (sender == _radioAl)
            {
                _txtBlockName.Text = "Name: Al";
            }
        }

        /// <summary>
        /// Erases the default text "Amount" when the user first clicks on it 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AmountClear(object sender, RoutedEventArgs e)
        {
            if (_txtBoxAmount.Text == "Amount")
            {
                _txtBoxAmount.Text = "";
            }
        }

        /// <summary>
        /// Places bet when the "bets" button is pressed. Requires the player radio button to be chosen, the bet  amount to be entered, and the greyhound number to be chosen. Places bet using the method in the  guys class. Updates labels with current description.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPlaceBet(object sender, RoutedEventArgs e)
        {
            switch(_txtBlockName.Text)
            {
                case "Name: Joe":
                    _guys[0].PlaceBet(int.Parse(_txtBoxAmount.Text), _comboDogChoice.SelectedIndex+1);
                    if(_guys[0].HasPlacedBet == true)
                    {
                        _txtBetJoe.Text = _guys[0].Bet.GetDescription();
                    }
                    break;
                case "Name: Bob":
                    _guys[1].PlaceBet(int.Parse(_txtBoxAmount.Text), _comboDogChoice.SelectedIndex+1);
                    if (_guys[1].HasPlacedBet == true)
                    {
                        _txtBetBob.Text = _guys[1].Bet.GetDescription();
                    }
                    break;
                case "Name: Al":
                    _guys[2].PlaceBet(int.Parse(_txtBoxAmount.Text), _comboDogChoice.SelectedIndex+1);
                    if (_guys[2].HasPlacedBet == true)
                    {
                        _txtBetAl.Text = _guys[2].Bet.GetDescription();
                    }
                    break;
                    // If invalid information is entered, the program will notify the user.
                default:
                    Debug.Assert(false, "Invalid Information");
                    break;
            }

            // Only when all 3 player have placed some sort of bet can the race begin
            if (_txtBetJoe.Text != "Joe's Bet:" & _txtBetBob.Text != "Bob's Bet:" & _txtBetAl.Text != "Al's Bet:")
            { _btnRace.IsEnabled = true; }

        }

        private void OnRace(object sender, RoutedEventArgs e)
        {
            while (_greyhounds[0].CurrentLocation< 1280)
            {
                for (int _greyhoundIndex = 0; _greyhoundIndex < 4; _greyhoundIndex++)
                {
                    _greyhounds[_greyhoundIndex].CurrentLocation += _greyhounds[_greyhoundIndex].RandomSteps.Next(100, 200);
                    switch(_greyhoundIndex)
                    {
                        case 0:
                            Canvas.SetLeft(_imgGreyhound1, Canvas.GetLeft(_imgGreyhound1) + 100);
                            break;
                        case 1:
                            Canvas.SetLeft(_imgGreyhound2, _greyhounds[_greyhoundIndex].CurrentLocation);
                            break;
                        case 2:
                            Canvas.SetLeft(_imgGreyhound3, _greyhounds[_greyhoundIndex].CurrentLocation);
                            break;
                        case 3:
                            Canvas.SetLeft(_imgGreyhound4, _greyhounds[_greyhoundIndex].CurrentLocation);
                            break;
                    }
                }
            }
        }
    }
}
